package kiuziu.aufgabengeber;

public class Parameter {

	private PseudoClass pseudoClass;
	private boolean attributeAccess;
	private boolean fullConstructor;
	private boolean getter;
	private boolean setter;
	private boolean useInitialValues;

	public Parameter(
			PseudoClass pseudoClass, 
			boolean attributeAccess, 
			boolean fullConstructor, 
			boolean getter,
			boolean setter, 
			boolean useInitialValues) {
		this.pseudoClass = pseudoClass;
		this.attributeAccess = attributeAccess;
		this.fullConstructor = fullConstructor;
		this.getter = getter;
		this.setter = setter;
		this.useInitialValues = useInitialValues;
	}

	public PseudoClass getPseudoClass() {
		return pseudoClass;
	}

	public boolean getAttributeAccess() {
		return attributeAccess;
	}

	public boolean getFullConstructor() {
		return fullConstructor;
	}

	public boolean getGetter() {
		return getter;
	}

	public boolean getSetter() {
		return setter;
	}

	public boolean getUseInitialValues() {
		return useInitialValues;
	}
}
