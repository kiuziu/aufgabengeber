package kiuziu.aufgabengeber;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

public class ClassGenerator {
	private static Names names = new Names();
	private static List<Attribute> possibleAttributes = names.getPossibleAttributes();
	private static List<String> possibleClassNames = names.getPossibleClassNames();
	private static Map<String, Integer> map = new HashMap<>();

	public ClassGenerator() {
	}

	public static PseudoClass generateClass(String className, Attribute[] attributes, boolean simpleConstructor,
			boolean fullConstructor, boolean getter, boolean setter) {
		PseudoClass pseudoClass = new PseudoClass();

		// possibly add number to class name
		String actualClassName = className;
		if (map.containsKey(actualClassName)) {
			int i = map.get(actualClassName);
			i++;
			map.put(actualClassName, i);
			actualClassName = actualClassName + i;
		} else {
			map.put(actualClassName, 0);
		}

		// set class name
		pseudoClass.setName(actualClassName);

		// set attributes
		for (Attribute a : attributes) {
			pseudoClass.addAttribute(a);
		}

		// choose empty constructor
		if (simpleConstructor) {
			pseudoClass.addConstructor(new Constructor(actualClassName));
		}

		// choose full constructor
		if (fullConstructor) {
			pseudoClass.addConstructor(new Constructor(actualClassName, attributes));
		}

		// choose getter methods
		if (getter) {
			for (Attribute a : attributes) {
				String methodName = "get" + (a.name).substring(0, 1).toUpperCase() + a.name.substring(1);
				Method m = new Method(methodName, a.dataType, null);
				pseudoClass.addMethod(m);
			}
		}

		// choose setter methods
		if (setter) {
			for (Attribute a : attributes) {
				String methodName = "set" + (a.name).substring(0, 1).toUpperCase() + a.name.substring(1);
				Method m = new Method(methodName, "void", new Attribute[] { a });
				pseudoClass.addMethod(m);
			}
		}

		// output
		System.out.println(pseudoClass.asDiagram(true));
		return pseudoClass;
	}

	// generate test class

	public static PseudoClass generateTestClass() {
		return generateClass("Paket", new Attribute[] { new Attribute("typ", "char"), new Attribute("name", "String"),
				new Attribute("bunt", "boolean"), new Attribute("alter", "int") }, true, true, true, true);
	}

	// generate random classes

	public static PseudoClass[] generateRandomClasses(int numberOfClasses, int maxNumberOfAttributes,
			boolean simpleConstructor, boolean fullConstructor, boolean getter, boolean setter) {
		List<String> selectedClassNames = new LinkedList<>();
		PseudoClass[] classes = new PseudoClass[numberOfClasses];

		for (int i = 0; i < numberOfClasses; i++) {
			int numberOfAttributes = (int) (Math.random() * maxNumberOfAttributes) + 1;
			String name = possibleClassNames.remove((int) (Math.random() * possibleClassNames.size()));
			classes[i] = generateRandomClass(name, numberOfAttributes, simpleConstructor, fullConstructor, getter,
					setter);
			selectedClassNames.add(name);
		}

		possibleClassNames.addAll(selectedClassNames);
		return classes;
	}

	public static PseudoClass generateRandomClass(int numberOfAttributes, boolean simpleConstructor,
			boolean fullConstructor, boolean getter, boolean setter) {
		String name = possibleClassNames.get((int) (Math.random() * possibleClassNames.size()));
		return generateRandomClass(name, numberOfAttributes, simpleConstructor, fullConstructor, getter, setter);
	}

	public static PseudoClass generateRandomClass(String className, int numberOfAttributes, boolean simpleConstructor,
			boolean fullConstructor, boolean getter, boolean setter) {
		Attribute[] attributes = selectRandomAttributes(numberOfAttributes);
		return generateClass(className, attributes, simpleConstructor, fullConstructor, getter, setter);
	}

	// helper

	private static Attribute[] selectRandomAttributes(int numberOfAttributes) {
		List<Attribute> selectedAttributes = new LinkedList<>();
		Attribute[] attributes = new Attribute[numberOfAttributes];
		for (int i = 0; i < numberOfAttributes; i++) {
			Attribute a = possibleAttributes.remove((int) (Math.random() * possibleAttributes.size()));
			selectedAttributes.add(a);
			attributes[i] = a;
		}
		possibleAttributes.addAll(selectedAttributes);
		return attributes;
	}

}
