package kiuziu.aufgabengeber;

import java.util.List;
import java.util.LinkedList;

public class Names {

	private List<Attribute> possibleAttributes = new LinkedList<>();
	private List<String> possibleClassNames = new LinkedList<>();

	public Names() {
		fillAttributeList();
		fillClassNameList();
	}

	private void fillClassNameList() {
		addClassName("Mensch");
		addClassName("Held");
		addClassName("Musikinstrument");
		addClassName("Pokemon");
		addClassName("Fahrzeug");
		addClassName("Paket");
		addClassName("Raumschiff");
		addClassName("Crewmitglied");
		addClassName("Gitarre");
		addClassName("Musikgruppe");
		addClassName("Monster");
	}

	private void fillAttributeList() {
		addAttribute(new Attribute("haeufigkeit", "double"));
		addAttribute(new Attribute("stufe", "int"));
		addAttribute(new Attribute("alter", "int"));
		addAttribute(new Attribute("name", "String"));
		addAttribute(new Attribute("abteilung", "String"));
		addAttribute(new Attribute("postleitzahl", "int"));
		addAttribute(new Attribute("groesse", "int"));
		addAttribute(new Attribute("typ", "char"));
		addAttribute(new Attribute("bunt", "boolean"));
		// addAttribute( new Attribute("", ""));
	}

	public List<String> getPossibleClassNames() {
		return possibleClassNames;
	}

	public List<Attribute> getPossibleAttributes() {
		return possibleAttributes;
	}

	private void addClassName(String name) {
		if (DataTypes.isLegalClassName(name)) {
			possibleClassNames.add(name);
		}
	}

	private void addAttribute(Attribute a) {
		if (DataTypes.isLegalAttribute(a)) {
			possibleAttributes.add(a);
		}
	}

}
