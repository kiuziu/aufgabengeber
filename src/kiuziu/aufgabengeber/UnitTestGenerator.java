package kiuziu.aufgabengeber;

public class UnitTestGenerator
{
    static FileWriter writer = new FileWriter();
    static StringBuilder sb = new StringBuilder();

    private UnitTestGenerator(){}

    static void saveUnitTest(Parameter p) {
    	
        sb = new StringBuilder();

        addIntro(p);
        
        if (p.getAttributeAccess()) {
            addAttributes(p);
        }
        
        if (p.getUseInitialValues()) {            
            addInitialisation(p);         
        }
        
        if (p.getFullConstructor()) {
            addFullConstructor(p);
        }
        
        if (p.getSetter()) {
            addSetters(p);
        }
        
        if (p.getGetter()) {
            addGetters(p);
        }
        
        addOutro();

        writer.createFile(p.getPseudoClass().getName()+"Test.java", sb);        
    }

    static private void addIntro(Parameter p) {

        addLine("import static org.junit.Assert.*;");
        addLine("import org.junit.After;");
        addLine("import org.junit.Before;");
        addLine("import org.junit.Test;");
        addLine("");

        addLine("public class "+p.getPseudoClass().getName()+"Test {");
        addLine("");

        addLine("  "+p.getPseudoClass().getName()+" testObject;");
        if(attributesContains(p, "double")) {
        	addLine("  double delta = 0.00000000000000001;");
        }                             
        addLine("");        

        addLine("  public "+p.getPseudoClass().getName()+"Test() {");
        addLine("  }");
        addLine("");

        addLine("  @Before");
        addLine("  public void setUp() {");

        for(Constructor c : p.getPseudoClass().getConstructors()) {
            if (c.getArguments().length==0) { // clumsy
                addLine("    testObject = new "+p.getPseudoClass().getName()+"();");
            }
        }

        addLine("  }");
        addLine("");

        addLine("  @After");
        addLine("  public void tearDown() {");
        addLine("    testObject=null;");
        addLine("  }");
        addLine("");
    }

	private static boolean attributesContains(Parameter p, String dataType) {
		for(Attribute a : p.getPseudoClass().getAttributes()) {
			if(a.dataType.equals(dataType)) return true;
		}
		return false;
	}

	static private void addInitialisation(Parameter p) {
    	
        if(p.getUseInitialValues() && p.getGetter()) {
            for (Attribute a : p.getPseudoClass().getAttributes()) {
                addLine("  @Test");
                addLine("  public void initialisierungVon"+(a.getName()).substring(0, 1).toUpperCase()+a.getName().substring(1)+"() {");
                if(a.getDataType().equals("double")) {
                	addLine("    assertEquals(testObject.get"+(a.getName()).substring(0, 1).toUpperCase()+a.getName().substring(1)+"(), "+a.getValue()+", delta);");
                }else {
                	addLine("    assertEquals(testObject.get"+(a.getName()).substring(0, 1).toUpperCase()+a.getName().substring(1)+"(), "+a.getValue()+");");
                }
                addLine("  }");
                addLine("");            
            }
        }
        else if (p.getUseInitialValues() && p.getAttributeAccess()) {
            for (Attribute a : p.getPseudoClass().getAttributes()) {
                addLine("  @Test");
                addLine("  public void initialisierungVon"+(a.getName()).substring(0, 1).toUpperCase()+a.getName().substring(1)+"2() {");
                if(a.getDataType().equals("double")) {
                	addLine("    assertEquals(testObject."+a.getName()+", "+a.getValue()+", delta);");
                }else {
                	addLine("    assertEquals(testObject."+a.getName()+", "+a.getValue()+");");
                }              
                addLine("  }");
                addLine("");            
            }
        }
        else {
            // sorry, no way to access attribute values
        }
    }

    static private void addSetters(Parameter p) {
        // Anlegen von Tests, die setter Testen (evtl. unter Benutzung von gettern)

        if (p.getSetter() && p.getAttributeAccess()) {
            for (Method m : p.getPseudoClass().getMethods()) {
                if (m.getName().subSequence(0, 3).equals("set")) {
                    addLine("  @Test");
                    addLine("  public void "+m.getName()+"Test() {");    
                    // repeat test calls three times
                    for (int i=0; i<3; i++) {
                        String value = DataTypes.getRandomExampleValue(m.getArguments()[0].getDataType());
                        addLine("    testObject."+m.getName()+"("+value+");");
                        
                        addLine("    assertEquals("+value+", testObject."+m.getName().substring(3,4).toLowerCase()+m.getName().substring(4)+");");
                    }
                    addLine("  }");
                    addLine("");            
                }    
            }
        }
        else if (p.getSetter() && p.getGetter()) {
            for (Method m : p.getPseudoClass().getMethods()) {
                if (m.getName().subSequence(0, 3).equals("set")) {
                    addLine("  @Test");
                    addLine("  public void "+m.getName()+"Test () {");

                    // repeat test calls three times
                    for (int i=0; i<3; i++) {
                        String value = DataTypes.getRandomExampleValue(m.getArguments()[0].getDataType());
                        addLine("    testObject."+m.getName()+"("+value+");");
                        if(m.getArguments()[0].getDataType().equals("double")) {
                        	addLine("    assertEquals("+value+", testObject."+"get"+m.getName().subSequence(3, m.getName().length())+"(), delta);");
                        }else {
                        	addLine("    assertEquals("+value+", testObject."+"get"+m.getName().subSequence(3, m.getName().length())+"());");
                        }           
                    }
                    addLine("  }");
                    addLine("");            
                }    
            }
        }
        else if (p.getSetter()) {
            // test if setter methods at least has correct argument type
            for (Method m : p.getPseudoClass().getMethods()) {
                if (m.getName().subSequence(0, 3).equals("set")) {
                    addLine("  @Test");
                    addLine("  public void "+m.getName()+"Test( ) {");    
                    String value = DataTypes.getSafeRandomExampleValue(m.getArguments()[0].getDataType());
                    addLine("    testObject."+m.getName()+"("+value+");");
                    addLine("  }");
                    addLine("");            
                }    
            }
        }
        else {
            // keine Tests moeglich
        }
    }        

    static private void addGetters(Parameter p) {
    	
        if (p.getGetter() && p.getAttributeAccess()) {
            for (Method m : p.getPseudoClass().getMethods()) {
                if (m.getName().subSequence(0, 3).equals("get")) {
                    addLine("  @Test");
                    addLine("  public void "+m.getName()+"Test() {");    
                    // repeat test calls three times
                    for (int i=0; i<3; i++) {
                        String value = DataTypes.getRandomExampleValue(m.getReturnType());
                        addLine("    testObject."+m.getName().substring(3,4).toLowerCase()+m.getName().substring(4)+" = "+value+";");   
                        if(m.getReturnType().equals("double")) {
                        	addLine("    assertEquals("+value+", testObject."+m.getName()+"(), delta);");
                        }else {
                        	addLine("    assertEquals("+value+", testObject."+m.getName()+"());");
                        }                   
                    }
                    addLine("  }");
                    addLine("");            
                }    
            }        
        }
        else if (p.getGetter() && p.getSetter()) {
            for (Method m : p.getPseudoClass().getMethods()) {
                if (m.getName().subSequence(0, 3).equals("get")) {
                    addLine("  @Test");
                    addLine("  public void "+m.getName()+"Test() {");    
                    // repeat test calls three times
                    for (int i=0; i<3; i++) {
                        String value = DataTypes.getRandomExampleValue(m.getReturnType());
                        addLine("    testObject.s"+m.getName().substring(1)+"("+value+");");
                        if(m.getReturnType().equals("double")) {
                        	addLine("    assertEquals("+value+", testObject."+m.getName()+"(), delta);");
                        }else {
                        	addLine("    assertEquals("+value+", testObject."+m.getName()+"());");
                        }                      
                    }
                    addLine("  }");
                    addLine("");            
                }    
            }        
        }
        else if (p.getGetter()) {
            for (Method m : p.getPseudoClass().getMethods()) {
                if (m.getName().subSequence(0, 3).equals("get")) {
                    addLine("  @Test");
                    addLine("  public void "+m.getName()+"Test( ) {");    
                    addLine("    "+m.getReturnType()+" test = testObject."+m.getName()+"();");                        
                    addLine("  }");
                    addLine("");            
                }    
            }        
        }
        else {
            // keine Tests moeglich
        }
    }

    static private void addFullConstructor(Parameter p) {
    	
        addLine("  @Test");
        addLine("  public void constructor"+p.getPseudoClass().getName()+"() {");    

        StringBuilder sb = new StringBuilder();        
        for(Constructor c : p.getPseudoClass().getConstructors()) {
            Attribute [] args = c.getArguments();
            String [] values = new String [args.length];
            if (args.length>0) { // clumsy

                // repeat test calls three times
                for (int i=0; i<3; i++) {
                    
                    //calculate list of parameters
                    for (int j=0; j<args.length; j++) {
                        values[j] = DataTypes.getSafeRandomExampleValue(args[j].getDataType());
                        sb.append(values[j]);
                        if (j<c.getArguments().length-1) {
                            sb.append(", ");
                        }
                    }
                    addLine("    testObject = new "+p.getPseudoClass().getName()+"("+sb.toString()+");");
                    sb.delete(0, sb.length());
                    
                    if(p.getAttributeAccess()) {
                        for (int j=0; j<args.length; j++) {
                        	if(p.getPseudoClass().getAttributes().get(j).getDataType().equals("double")) {
                        		addLine("    assertEquals(testObject."+p.getPseudoClass().getAttributes().get(j).getName()+", "+values[j]+", delta);");
                        	}else {
                        		addLine("    assertEquals(testObject."+p.getPseudoClass().getAttributes().get(j).getName()+", "+values[j]+");"); 
                        	}                                          
                        }
                    }
                    else if (p.getGetter()) {
                        for (int j=0; j<args.length; j++) {
                            String attributeName = p.getPseudoClass().getAttributes().get(j).getName();
                            if(p.getPseudoClass().getAttributes().get(j).getDataType().equals("double")) {
                            	addLine("    assertEquals(testObject.get"+attributeName.substring(0,1).toUpperCase()+attributeName.substring(1)+"(), "+values[j]+", delta);");  
                            }else {
                            	addLine("    assertEquals(testObject.get"+attributeName.substring(0,1).toUpperCase()+attributeName.substring(1)+"(), "+values[j]+");");  
                            }                    
                        }                    
                    }
                    else {
                        // no way of checking
                    }
                }                
                break;
            }
        }

        addLine("  }");
        addLine("");            
    }

    static private void addAttributes(Parameter p) {
    	
        addLine("  @Test");
        addLine("  public void attributeUeberpruefen() {");         
        for(Attribute a : p.getPseudoClass().getAttributes()) {
            addLine("    testObject."+a.getName()+" = "+DataTypes.getRandomExampleValue(a.getDataType())+";");                        
        }

        addLine("  }");
        addLine("");            
    }

    static private void addOutro() {
    	    // end class frame
        addLine("}");
    }    

    static private void addLine(String s) {
        sb.append(s+"\n");
    }

}
