package kiuziu.aufgabengeber;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileWriter {
	
	public FileWriter() {
	}

	void createFile(String fileName, StringBuilder sb) {
		createFile(fileName, sb.toString());
	}

	void createFile(String fileName, String fileContent) {
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "utf-8"));
			writer.write(fileContent);
		} catch (IOException ex) {
			// report
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				/* ignore */}
		}

	}
}