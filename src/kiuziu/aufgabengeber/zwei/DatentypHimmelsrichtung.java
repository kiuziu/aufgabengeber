package kiuziu.aufgabengeber.zwei;

public class DatentypHimmelsrichtung extends DatentypChar {

	public DatentypHimmelsrichtung() {
	}

	@Override
	String getRandomValue() {
		return Tools.randomOfAny("'N'", "'S'", "'E'", "'W'");
	}
}
