package kiuziu.aufgabengeber.zwei;


public class IntBetrag extends Methode
{

    public IntBetrag()
    {
        super("betrag", new DatentypInt(), new DatentypInt() );
        testname = "betragInt";
    }

    @Override
	String beschreibungGeben() {
        String s = "Die Methode '"+this.alsUML()+"' nimmt ein int als Eingangsargument \r\n";
        s = s + "und gibt den Betrag davon zurueck.";
        return s;
    }

    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<5; i++) {
            int value = Integer.parseInt(argumente[0].getRandomValue());
            String expected = ""+value;
            if (value<0) expected = ""+(value*-1); 
            String actual = "testObject."+this.name+"("+value+")";
            sb.append("    "+returnTyp.generateAssertionCode(expected, actual));            
        }
        return sb.toString();
    }   

}
