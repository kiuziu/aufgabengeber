package kiuziu.aufgabengeber.zwei;

import java.util.List;
import java.util.LinkedList;

public class ZufallsVorrat
{
    private List<Attribut> moeglicheAttribute = new LinkedList<>();
    private List<String> moeglicheKlassennamen = new LinkedList<>();
    private List<Methode> moeglicheExtramethoden = new LinkedList<>();

    public ZufallsVorrat()
    {
        fillAttributeList();
        fillClassNameList();
        fillExtraMethodsList();
    }

    private void fillClassNameList() {
        moeglicheKlassennamen.add( "Mensch" );
        moeglicheKlassennamen.add( "Held" );
        moeglicheKlassennamen.add( "Musikinstrument" );
        moeglicheKlassennamen.add( "Pokemon" );
        moeglicheKlassennamen.add( "Fahrzeug" );
        moeglicheKlassennamen.add( "Paket" );
        moeglicheKlassennamen.add( "Raumschiff" );
        moeglicheKlassennamen.add( "Crewmitglied" );
        moeglicheKlassennamen.add( "Musikgruppe" );
        moeglicheKlassennamen.add( "Monster" );
    }

    private void fillAttributeList() {
        addAttribut( "himmelsrichtung", new DatentypHimmelsrichtung());
        addAttribut( "haeufigkeit", new DatentypDouble());
        addAttribut( "wert", new DatentypDouble());
        addAttribut( "stufe", new DatentypInt());
        addAttribut( "alter", new DatentypInt());
        addAttribut( "name", new DatentypNachname());
        addAttribut( "abteilung", new DatentypString());
        addAttribut( "postleitzahl", new DatentypPlz());
        addAttribut( "typ", new DatentypChar());
        addAttribut( "bunt", new DatentypBoolean());
        addAttribut( "wichtig", new DatentypBoolean());
        addAttribut( "haarfarbe", new DatentypHaarfarbe());
    }

    private void fillExtraMethodsList() {
        moeglicheExtramethoden.add( new IntVerdoppeln() );
        moeglicheExtramethoden.add( new IntBetrag() );
        moeglicheExtramethoden.add( new DoubleKubik() );        
        moeglicheExtramethoden.add( new IntGerade() );        
//         moeglicheExtramethoden.add( new IntArraySumme() );        
    }

    public List<String> getMoeglicheKlassennamen() {
        return moeglicheKlassennamen;
    }

    public List<Attribut> getMoeglicheAttribute() {
        return moeglicheAttribute;
    }

    public List<Methode> getMoeglicheExtramethoden() {
        return moeglicheExtramethoden;
    }

    private void addAttribut(String bezeichner, Datentyp d) {
        moeglicheAttribute.add( new Attribut(bezeichner,d) );
    }
    

}
