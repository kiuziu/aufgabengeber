package kiuziu.aufgabengeber.zwei;

public class DatentypInt extends Datentyp
{
    @Override
	String getRandomValue() {
        return ""+(int) (Math.random()*100-30);
    }

    @Override
	String getName() { return "int"; }

    @Override
	String getInitialValue(){
        return "0";
    }

}
