package kiuziu.aufgabengeber.zwei;

public class Attribut
{
    Datentyp datentyp;
    String bezeichner; 
    String attributwert;
    
    public Attribut(String b, Datentyp d)
    {
        datentyp = d;
        bezeichner = b;
        attributwert = d.getInitialValue();
    }
    
    String getBezeichner() { return bezeichner; }
    String getAttributwert() { return attributwert; }
    void setAttributwert(String  s) { attributwert=s; }
    Datentyp getDatentyp() { return datentyp; }

    String alsUML() { return bezeichner+": "+datentyp.getName(); }
    
}
