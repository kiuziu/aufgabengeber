package kiuziu.aufgabengeber.zwei;


public abstract class Datentyp
{

    abstract String getRandomValue();
    abstract String getInitialValue();    
    abstract String getName();
    
    String generateAssertionCode(String expected, String actual) {
        return "assertEquals("+expected+", "+actual+");\r\n"; 
    }

}
