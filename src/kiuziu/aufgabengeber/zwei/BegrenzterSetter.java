package kiuziu.aufgabengeber.zwei;


public class BegrenzterSetter extends Setter
{
    int untergrenze;
    int obergrenze;

    public BegrenzterSetter(String attributName, int u, int o) {
        super(attributName, new DatentypInt());
        untergrenze = u;
        obergrenze = o;
    }

    @Override
	String beschreibungGeben() {
        String s = "Die Methode "+this.alsUML()+" setzt das entsprechende Attribut des Objekts \r\n";
        s = s + "auf den Wert, der im Eingangsargument angegeben ist. Das gilt aber nur, \r\n";
        s = s + "wenn sich der zwischen "+untergrenze+" (einschliesslich) und "+obergrenze+" (*nicht* einschliesslich) \r\n";
        s = s + "befindet. Ansonsten bleibt der Wert einfach unveraendert.";
        return s;
    }
    
    private int getRandomBelow() { return (int) (Math.random()*(untergrenze-Integer.MIN_VALUE)+Integer.MIN_VALUE); }
    private int getRandomAbove() { return (int) (Math.random()*(Integer.MAX_VALUE-obergrenze)+obergrenze); }
    private int getRandomBetween() { return (int) (Math.random()*(obergrenze-untergrenze)+untergrenze); }

    
    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        String expected = "\"\"";
        String actual = "\"\"";

        if (!klasse.privateAccess) actual = "testObject."+attributName;
        else if (klasse.getters) actual = "testObject.get"+capitalise(attributName)+"()";
        
        int [] newValues = new int [6];
        int [] oldValues = new int [6];        
        
        for (int i=0; i<6; i++) oldValues[i] = getRandomBetween();        
        newValues[0] = getRandomBelow();
        newValues[1] = untergrenze-1;
        newValues[2] = getRandomBetween();
        newValues[3] = getRandomBetween();
        newValues[4] = obergrenze;
        newValues[5] = getRandomAbove();


        for (int i=0; i<6; i++) {
            sb.append("    testObject."+this.attributName+" = "+oldValues[i]+";\r\n"); // set legitimate value directly
            sb.append("    testObject."+this.name+"("+newValues[i]+");\r\n"); // call to method being tested
            if (i==2 || i==3)  expected = ""+newValues[i];
            else expected = ""+oldValues[i];
            if (klasse.getters || !klasse.privateAccess) {
                sb.append("    "+argumente[0].generateAssertionCode(expected, actual));
                sb.append("\r\n"); 
            }
        }
        return sb.toString();
    }   

}
