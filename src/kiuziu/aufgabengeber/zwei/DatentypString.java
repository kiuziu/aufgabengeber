package kiuziu.aufgabengeber.zwei;

public class DatentypString extends Datentyp {

	DatentypString() {
	}

	@Override
	String getName() {
		return "String";
	}

	@Override
	String getRandomValue() {
		return Tools.randomOfAny("\"Max\"", "\"Susi\"", "\"Shamira\"", "\"Anton\"", "\"Lydia\"", "\"London\"",
				"\"Paris\"", "\"Wendy\"", "\"Berlin\"", "\"Matterhorn\"");
	}

	@Override
	String getInitialValue() {
		return "null";
	}

}
