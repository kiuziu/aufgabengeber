package kiuziu.aufgabengeber.zwei;

public class KonstruktorMitVorbelegung extends Konstruktor
{

    public KonstruktorMitVorbelegung(Klasse k)
    {
        super(k.bezeichnerGeben(), k.attributeGeben());
        super.testname = k.bezeichnerGeben()+"KonstruktorMitInitialisierung";        
    }

    public KonstruktorMitVorbelegung(String klassenname, Attribut... a)
    {
        super(klassenname, a);
        super.testname = klassenname+"KonstruktorMitInitialisierung";        
    }

    @Override
    String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        sb.append("    testObject = new "+name+"();\r\n");

        for (int i=0; i<attribute.length; i++) {
            String expected = attribute[i].getAttributwert();
            String actual = "";

            if (!klasse.privateAccess) actual = "testObject."+attribute[i].getBezeichner();
            else if (klasse.getters) actual = "testObject.get"+capitalise(attribute[i].getBezeichner()+"()");
            else {
                sb.append("    // ohne Getter und mit privaten Attributen Keine Moeglichkeit, den Konstruktor sinnvoll weiter zu testen\r\n");
                return sb.toString();
            }
            
            sb.append("    "+attribute[i].getDatentyp().generateAssertionCode(expected, actual));            
        }

        return sb.toString();
    }

    @Override
    String alsUML() {
        return name + "()";
    }

    @Override
    public String beschreibungGeben() {
        StringBuilder sb = new StringBuilder();
        for (Attribut a : attribute) {
            sb.append("Der Wert des Attributs '"+a.getBezeichner() +"' soll nach Ausfuehrung \ndes Konstruktors ohne Argumente sein: "+a.getAttributwert()+".");
            sb.append("\r\n");
        }
        return sb.toString();
    }

}
