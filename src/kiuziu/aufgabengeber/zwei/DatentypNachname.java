package kiuziu.aufgabengeber.zwei;

public class DatentypNachname extends DatentypString {
	
	public DatentypNachname() {
	}

	@Override
	String getRandomValue() {
		return Tools.randomOfAny("\"Cabell\"", "\"Kim\"", "\"Mueller\"", "\"Gutierrez\"", "\"Spratt\"");
	}

}
