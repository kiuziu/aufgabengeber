package kiuziu.aufgabengeber.zwei;


public class Controller
{
    FileWriter writer = new FileWriter();

    public Controller() {
    }
    
    void aufgabeErzeugen(int numberOfAttributes, boolean withInitialisation, boolean fullConstructor, boolean getter, boolean setter, boolean extraMethods, boolean withComments, boolean privateAccess) {
        Klasse k = KlassenGenerator.generateRandomClass(
            numberOfAttributes,
            withInitialisation, 
            fullConstructor, 
            getter, 
            setter, 
            extraMethods, 
            privateAccess
            );      
            
        AufgabenErzeuger.erzeugeAufgabe(k, withComments);
    }
    
    
    public void aufgabeErzeugenKlasseMitRechenmethoden(String name) {
        Klasse k = KlassenGenerator.erzeugeKlasseMitRechenmethoden(name);
        AufgabenErzeuger.erzeugeAufgabe(k, true);
    }
    public void aufgabeErzeugenKlasseMitRechenmethodenIf(String name) {
        Klasse k = KlassenGenerator.erzeugeKlasseMitRechenmethodenIf(name);
        AufgabenErzeuger.erzeugeAufgabe(k, true);
    }
    public void aufgabeErzeugenKlasseMitRechenmethodenFor(String name) {
        Klasse k = KlassenGenerator.erzeugeKlasseMitRechenmethodenFor(name);
        AufgabenErzeuger.erzeugeAufgabe(k, true);
    }
    public void aufgabeErzeugeKlasseMitRechenmethodenEinfach(String name) {
        Klasse k = KlassenGenerator.erzeugeKlasseMitRechenmethodenEinfach(name);
        AufgabenErzeuger.erzeugeAufgabe(k, true);
    }

}
