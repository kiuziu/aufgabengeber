package kiuziu.aufgabengeber.zwei;

import java.net.URL;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class URLHelper {
	URL getURLNonstatic(Object o, String filename) {
		return o.getClass().getResource(filename);
	}

	URL getURLNonstatic(String filename) {
		return this.getClass().getClassLoader().getResource(filename);
	}

	public static URL getURL(Object o, String filename) {
		return getURLRelative(o, filename);
	}

	private static URL getURLRelative(Object o, String filename) {
		Class cls = o.getClass();
		return cls.getResource(filename);
	}

	public static URL getURL(String filename) {
		return getURLAbsolute(filename);
	}

	private static URL getURLAbsolute(String filename) {
		return new Object() {
		}.getClass().getClassLoader().getResource(filename);
	}

	static InputStream getInputStream(String filename) {
		return getInputStreamAbsolute(filename);
	}

	static InputStream getInputStream(Object o, String filename) {
		return getInputStreamRelative(o, filename);
	}

	private static InputStream getInputStreamAbsolute(String filename) {
		InputStream is = null;
		try {
			Class cls = new Object() {
			}.getClass();
			ClassLoader cLoader = cls.getClassLoader();
			is = cLoader.getResourceAsStream(filename);
		} catch (Exception e) {
			System.out.println(e);
		}
		if (is == null)
			System.out.println("URLHelper: Fehler!!!");
		return is;
	}

	private static InputStream getInputStreamRelative(Object o, String filename) {
		InputStream is = null;
		try {
			Class cls = o.getClass();
			is = cls.getResourceAsStream(filename);
		} catch (Exception e) {
			System.out.println(e);
		}
		if (is == null)
			System.out.println("URLHelper: Fehler!!!");
		return is;
	}

	public InputStream getInputStreamNonstaticRelative(Object o, String filename) {
		Class currentClass = o.getClass();
		InputStream resource = currentClass.getResourceAsStream(filename);
		return resource;
	}

	static BufferedReader getBufferedReaderFromFileName(String filename) {
		InputStream stream = URLHelper.getInputStream(filename);
		BufferedReader in = new BufferedReader(new InputStreamReader(stream));
		return in;
	}

	static BufferedReader getBufferedReaderFromFileName(Object o, String filename) {
		InputStream stream = URLHelper.getInputStream(o, filename);
		BufferedReader in = new BufferedReader(new InputStreamReader(stream));
		return in;
	}

	public String pfadZumOberverzeichnisNonstatic() {
		java.security.ProtectionDomain pd = getClass().getProtectionDomain();
		if (pd == null)
			return null;
		java.security.CodeSource cs = pd.getCodeSource();
		if (cs == null)
			return null;
		java.net.URL url = cs.getLocation();
		if (url == null)
			return null;
		java.io.File file = new java.io.File(url.getFile());
		String pfad = "";
		try {
			pfad = file.getParentFile().getAbsolutePath();
			pfad = java.net.URLDecoder.decode(pfad, "utf-8");
		} catch (Exception e) {
		}
		System.out.println("\nAkt. Verzeichnis: " + System.getProperty("user.dir"));
		System.out.println("Absoluter Pfad:  " + pfad);
		return pfad + "/";
	}

	public static String pfadZumOberverzeichnis() {
		URLHelper u = new URLHelper();
		return u.pfadZumOberverzeichnisNonstatic();
	}

	public static String pfadZumVerzeichnis() {
		URLHelper u = new URLHelper();
		return u.pfadZumVerzeichnisNonstatic();
	}

	public String pfadZumVerzeichnisNonstatic() {
		java.security.ProtectionDomain pd = getClass().getProtectionDomain();
		if (pd == null)
			return null;
		java.security.CodeSource cs = pd.getCodeSource();
		if (cs == null)
			return null;
		java.net.URL url = cs.getLocation();
		if (url == null)
			return null;
		java.io.File file = new java.io.File(url.getFile());
		String pfad = "";
		try {
			pfad = file.getAbsolutePath();
			pfad = java.net.URLDecoder.decode(pfad, "utf-8");
		} catch (Exception e) {
		}
		System.out.println("\nAkt. Verzeichnis: " + System.getProperty("user.dir"));
		System.out.println("Absoluter Pfad:  " + pfad);
		return pfad + "/";
	}
}
