package kiuziu.aufgabengeber.zwei;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

public class KlassenGenerator {
	private static ZufallsVorrat vorrat = new ZufallsVorrat();
	private static List<Attribut> moeglicheAttribute = vorrat.getMoeglicheAttribute();
	private static List<String> moeglicheKlassennamen = vorrat.getMoeglicheKlassennamen();
	private static List<Methode> moeglicheMethoden = vorrat.getMoeglicheExtramethoden();
	private static Map<String, Integer> map = new HashMap<>();

	// generate class

	public static Klasse generateClass(String className, Attribut[] attribute, boolean withInitialisation,
			boolean fullConstructor, boolean getter, boolean setter, boolean extraMethods) {
		return generateClass(className, attribute, withInitialisation, fullConstructor, getter, setter, extraMethods,
				false);
	}

	public static Klasse generateClass(String className, Attribut[] attribute, boolean withInitialisation,
			boolean fullConstructor, boolean getter, boolean setter, boolean extraMethods, boolean privateAccess) {
		String newClassName = className;
		// possibly add number to class name
		if (map.containsKey(newClassName)) {
			int i = map.get(newClassName);
			i++;
			map.put(newClassName, i);
			newClassName = newClassName + i;
		} else {
			map.put(newClassName, 0);
		}

		// set class name
		Klasse klasse = new Klasse(newClassName);

		klasse.getters = getter;
		klasse.setters = setter;
		klasse.privateAccess = privateAccess;

		// set attributes
		for (Attribut a : attribute) {
			klasse.attributHinzufuegen(a);
		}

		// choose empty constructor
		klasse.ergaenzeKonstruktorOhneArgumente(withInitialisation);

		// choose full constructor
		if (fullConstructor) {
			klasse.ergaenzeKonstruktorMitArgumentenAusAttributen();
		}

		// choose getter methods
		if (getter) {
			klasse.ergaenzeGetterAusAttributen();
		}

		// choose setter methods
		if (setter) {
			klasse.ergaenzeSetterAusAttributen();
		}

		// choose extra methods
		if (extraMethods) {
			Methode[] methoden = selectRandomExtraMethods(1);
			for (Methode m : methoden) {
				klasse.methodeHinzufuegen(m);
			}
		}

		// output
		return klasse;
	}

	// generate random classes

	public static Klasse[] generateRandomClasses() {
		return generateRandomClasses(2);
	}

	public static Klasse[] generateRandomClasses(int numberOfClasses) {
		return generateRandomClasses(numberOfClasses, 3, true, true, true, true, true, false);
	}

	public static Klasse[] generateRandomClasses(int numberOfClasses, int maxNumberOfAttributes,
			boolean simpleConstructorPlus, boolean fullConstructor, boolean getter,
			boolean setter, boolean extraMethods, boolean privateAccess) {
		
		List<String> selectedClassNames = new LinkedList<>();
		Klasse[] klassen = new Klasse[numberOfClasses];

		for (int i = 0; i < numberOfClasses; i++) {
			int numberOfAttributes = (int) (Math.random() * maxNumberOfAttributes) + 1;
			String name = moeglicheKlassennamen.remove((int) (Math.random() * moeglicheKlassennamen.size()));
			klassen[i] = generateRandomClass(name, numberOfAttributes, simpleConstructorPlus, fullConstructor, getter,
					setter, extraMethods, privateAccess);
			selectedClassNames.add(name);
		}

		moeglicheKlassennamen.addAll(selectedClassNames);
		return klassen;
	}

	public static Klasse generateRandomClass() {
		return generateRandomClass((int) (Math.random() * 3 + 1), true, true, true, true, true, false);
	}

	public static Klasse generateRandomClass(int numberOfAttributes) {
		return generateRandomClass(numberOfAttributes, true, true, true, true, true, false);
	}

	public static Klasse generateRandomClass(int numberOfAttributes, boolean simpleConstructorPlus,
			boolean fullConstructor, boolean getter, boolean setter, boolean extraMethods, boolean privateAccess) {
		String name = moeglicheKlassennamen.get((int) (Math.random() * moeglicheKlassennamen.size()));
		return generateRandomClass(name, numberOfAttributes, simpleConstructorPlus, fullConstructor, getter, setter,
				extraMethods, privateAccess);
	}

	public static Klasse generateRandomClass(String className, int numberOfAttributes, boolean simpleConstructorPlus,
			boolean fullConstructor, boolean getter, boolean setter, boolean extraMethods, boolean privateAccess) {
		Attribut[] attributes = selectRandomAttributes(numberOfAttributes);
		return generateClass(className, attributes, simpleConstructorPlus, fullConstructor, getter, setter,
				extraMethods, privateAccess);
	}

	public static Klasse erzeugeKlasseMitRechenmethoden(String name) {
		Klasse k = new Klasse(name);
		k.methodeHinzufuegen(new IntArraySumme());
		k.methodeHinzufuegen(new DoubleKubik());
		k.methodeHinzufuegen(new IntVerdoppeln());
		k.methodeHinzufuegen(new IntBetrag());
		k.methodeHinzufuegen(new IntGerade());
		return k;
	}

	public static Klasse erzeugeKlasseMitRechenmethodenIf(String name) {
		Klasse k = new Klasse(name);
		k.methodeHinzufuegen(new IntBetrag());
		k.methodeHinzufuegen(new IntGerade());
		return k;
	}

	public static Klasse erzeugeKlasseMitRechenmethodenFor(String name) {
		Klasse k = new Klasse(name);
		k.methodeHinzufuegen(new IntArraySumme());
		return k;
	}

	public static Klasse erzeugeKlasseMitRechenmethodenEinfach(String name) {
		Klasse k = new Klasse(name);
		k.methodeHinzufuegen(new DoubleKubik());
		k.methodeHinzufuegen(new IntVerdoppeln());
		return k;
	}

	// helper

	private static Attribut[] selectRandomAttributes(int numberOfAttributes) {
		List<Attribut> selectedAttributes = new LinkedList<>();
		Attribut[] attributes = new Attribut[numberOfAttributes];
		for (int i = 0; i < numberOfAttributes; i++) {
			Attribut a = moeglicheAttribute.remove((int) (Math.random() * moeglicheAttribute.size()));
			selectedAttributes.add(a);
			attributes[i] = a;
		}
		moeglicheAttribute.addAll(selectedAttributes);
		return attributes;
	}

	private static Methode[] selectRandomExtraMethods(int numberOfMethods) {
		List<Methode> selectedMethods = new LinkedList<>();
		Methode[] methods = new Methode[numberOfMethods];
		for (int i = 0; i < numberOfMethods; i++) {
			Methode a = moeglicheMethoden.remove((int) (Math.random() * moeglicheMethoden.size()));
			selectedMethods.add(a);
			methods[i] = a;
		}
		moeglicheMethoden.addAll(selectedMethods);
		return methods;
	}

}
