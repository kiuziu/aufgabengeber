package kiuziu.aufgabengeber.zwei;


public class DatentypVoid extends Datentyp
{

    @Override
    String getRandomValue() {
        return "void";
    }

    @Override
    String getName() { return "void"; }

    @Override
    String getInitialValue(){
        return "void";
    }
}
