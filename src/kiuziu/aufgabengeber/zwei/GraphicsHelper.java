package kiuziu.aufgabengeber.zwei;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.HashMap;

import javax.swing.ImageIcon;


public class GraphicsHelper
{
    static boolean useCache = false;

    static HashMap<String, Image> imagesCache = new HashMap<>();    
    static HashMap<String, ImageIcon> imageIconsCache = new HashMap<>();    

    /**
     * Hilfsmethode zum sicheren Erzeugen eines Image aus einem Dateinamen.
     */
    public static Image createImage(String filename) {
        if (useCache) {
            Image img = imagesCache.get(filename);
            if (img!=null) return img;
        }
        
        URL url = URLHelper.getURL(filename);
        BufferedImage image = createFallbackImage();
        try {
            image = javax.imageio.ImageIO.read(url);
            if (useCache) {
                imagesCache.put(filename, image);
            }
        }
        catch (Exception e) {
            System.out.println(filename);
            System.out.println("Ein Bild unter diesem Namen existiert nicht, stattdessen wird ein leeres Bild erzeugt.");
            System.out.println("Error in GraphicsHelper: "+e);
        }
        return image;
    }    

    private static BufferedImage createFallbackImage() {
        return new BufferedImage(50,50,BufferedImage.TYPE_INT_ARGB);
    }    

    /**
     * Hilfsmethode zum sicheren Erzeugen eines ImageIcon aus einem Dateinamen.
     */
    public static ImageIcon createImageIcon(String filename) { 
        if (useCache) {
            ImageIcon iconOld = imageIconsCache.get(filename);
            if (iconOld!=null) return iconOld;
        }
        
        URL url = URLHelper.getURL(filename);
        if (url != null) {
            ImageIcon icon = new ImageIcon(url);
            if (useCache) {
                imageIconsCache.put(filename, icon);
            }
            return icon;
        }
		System.out.println(filename);
		System.out.println("Ein Bild unter diesem Namen existiert nicht, stattdessen wird ein leeres Bild erzeugt.");
		return new ImageIcon(createFallbackImage());
    }
    
    public static ImageIcon imageToImageIcon(Image image) {
        return new ImageIcon(image);
    }    

    public static Image resizeImage(Image image, int x, int y) {
    	Image newImage = image;
        if (image == null) {
            System.out.println("Diese Bild existiert nicht, stattdessen wird ein leeres Bild erzeugt.");
            newImage = createFallbackImage();
        }
        return newImage.getScaledInstance(x, y, java.awt.Image.SCALE_SMOOTH);
    }

    public static ImageIcon resizeImageIcon(ImageIcon icon, int x, int y) {
    	ImageIcon newIcon = icon;
        if (icon == null) newIcon = new ImageIcon(createFallbackImage());
        Image image = newIcon.getImage();
        image = resizeImage(image, x, y);
        return new ImageIcon(image);
    }

    

}
