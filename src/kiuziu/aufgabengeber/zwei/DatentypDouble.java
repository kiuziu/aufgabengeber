package kiuziu.aufgabengeber.zwei;

public class DatentypDouble extends Datentyp {
	String delta = "0.0005";

	public DatentypDouble() {
	}

	@Override
	String getName() {
		return "double";
	}

	@Override
	String getInitialValue() {
		return "0.0";
	}

	@Override
	String getRandomValue() {
		String result = "" + (Math.random() * 100 - 30);
		return result.substring(0, 5);
	}

	@Override
	String generateAssertionCode(String expected, String actual) {
		return "assertEquals(" + expected + ", " + actual + ", " + delta + ");\r\n";
	}

}
