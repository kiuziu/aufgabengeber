package kiuziu.aufgabengeber.zwei;

import java.util.List;
import java.util.ArrayList;

public class Klasse extends Datentyp {
	String bezeichner;
	List<Methode> methoden;
	List<Attribut> attribute;

	boolean getters = true;
	boolean setters = true;
	boolean privateAccess = false;

	boolean leererKonstruktorHinzugefuegt = false; // damit es nur einen solchen gibt

	public Klasse(String bezeichner) {
		this.bezeichner = bezeichner;
		methoden = new ArrayList<>();
		attribute = new ArrayList<>();
	}

	void ergaenzeGetterUndSetterAusAttributen() {
		ergaenzeGetterAusAttributen();
		ergaenzeSetterAusAttributen();
	}

	void ergaenzeGetterAusAttributen() {
		getters = true;
		for (Attribut a : attribute) {
			Methode m = new Getter(a.getBezeichner(), a.getDatentyp());
			methodeHinzufuegen(m);
		}
	}

	void ergaenzeSetterAusAttributen() {
		setters = true;
		for (Attribut a : attribute) {
			Methode m = new Setter(a.getBezeichner(), a.getDatentyp());
			methodeHinzufuegen(m);
		}
	}

	void ergaenzeKonstruktorOhneArgumenteOhneVorbelegung() {
		if (leererKonstruktorHinzugefuegt)
			return;
		leererKonstruktorHinzugefuegt = true;

		methodeHinzufuegen(new Konstruktor(bezeichner));
	}

	void ergaenzeKonstruktorOhneArgumenteMitVorbelegung() {
		if (leererKonstruktorHinzugefuegt)
			return;
		leererKonstruktorHinzugefuegt = true;

		for (Attribut a : attribute) {
			a.setAttributwert(a.getDatentyp().getRandomValue());
		}
		methodeHinzufuegen(new KonstruktorMitVorbelegung(bezeichner, attributeGeben()));
	}

	void ergaenzeKonstruktorOhneArgumente(boolean mitVorbelegung) {
		if (mitVorbelegung)
			this.ergaenzeKonstruktorOhneArgumenteMitVorbelegung();
		else
			this.ergaenzeKonstruktorOhneArgumenteOhneVorbelegung();
	}

	void ergaenzeKonstruktorMitArgumentenAusAttributen() {
		if (attributeGeben().length == 0) {
			if (leererKonstruktorHinzugefuegt)
				return;
			leererKonstruktorHinzugefuegt = true;
		}
		methodeHinzufuegen(new Konstruktor(this));
	}

	void ergaenzeKonstruktoren(boolean initialisation) {
		this.ergaenzeKonstruktorOhneArgumente(initialisation);
		ergaenzeKonstruktorMitArgumentenAusAttributen();
	}

	String bezeichnerGeben() {
		return bezeichner;
	}

	void methodeHinzufuegen(Methode m) {
		methoden.add(m);
		m.klasseSetzen(this);
	}

	void attributHinzufuegen(Attribut a) {
		attribute.add(a);
	}

	void attributHinzufuegen(String attributName, Datentyp d) {
		attribute.add(new Attribut(attributName, d));
	}

	List<Methode> methodenGeben() {
		return methoden;
	}

	Attribut[] attributeGeben() {
		return attribute.toArray(new Attribut[0]);
	}

	@Override
	String getRandomValue() {
		return "new " + bezeichner + "()";
	}

	@Override
	String getInitialValue() {
		return "null";
	}

	@Override
	String getName() {
		return bezeichner;
	}

	@Override
	String generateAssertionCode(String expected, String actual) {
		return "assertNotNull(" + actual + ");\r\n";
	}

}
