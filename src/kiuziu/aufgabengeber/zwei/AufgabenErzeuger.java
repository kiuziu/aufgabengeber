package kiuziu.aufgabengeber.zwei;

public class AufgabenErzeuger
{
    static void erzeugeAufgabe(Klasse k, boolean mitBeschreibung) {
        
        String testCode = gibTestklasse(k);
        FileWriter.createFile(k.bezeichnerGeben()+"Test.java", testCode);
        
        String aufgabe = gibAufgabenblatt(k, mitBeschreibung);
        FileWriter.createFile(k.bezeichnerGeben()+"Diagramm.txt", aufgabe);
    }

    static String gibTestklasse(Klasse k) {        
        return UnitTestErzeuger.erzeugeUnitTest(k);
    }    
        
    static String gibAufgabenblatt(Klasse k, boolean mitBeschreibung) {        
        StringBuilder sb = new StringBuilder();
        
        sb.append("Aufgabe: Setze das folgende Klassendiagramm in eine Java-Klasse um.\r\n");
        sb.append("Beachte dabei auch eventuelle zusaetzliche Angaben unter dem Diagramm.\r\n");
        sb.append("\r\n");
        
        sb.append( KlassendiagrammErzeuger.erzeugeDiagramm(k));

        if (mitBeschreibung) {
            sb.append("\r\n\r\n");
            for (Methode m : k.methodenGeben()) {
                String s = m.beschreibungGeben();
                if (s.length()>0) {
                    sb.append(s);
                    sb.append("\r\n\r\n");
                }
            }
        }
        System.out.println("AufgabenErzeuger:\r\n");
        System.out.println(sb.toString());

        return sb.toString();        
    }

}
