package kiuziu.aufgabengeber.zwei;

public class Konstruktor extends Methode
{
    Attribut [] attribute;    
    
    // erzeugt Konstruktor mit Argumenten, für jedes Attribut eines
    public Konstruktor(Klasse k) {
        this(k.getName(), k.attributeGeben());
        super.testname = k.getName()+"KonstruktorMitAllenArgumenten";
        this.klasse = k;
    }

    public Konstruktor(String klassenname, Attribut... a)
    {
        super(klassenname);
        super.testname = name+"KonstruktorMitArgumenten";
        attribute = a;
        if (a==null) attribute = new Attribut[0];
        
    }

    @Override
	String testzeilenErzeugen() {
        String[] values = new String[attribute.length]; // zur Speicherung der zufaelligen Attributwerte
        StringBuilder sb = new StringBuilder();
        sb.append("    testObject = new "+name+"(");
        
        for (int i=0; i<attribute.length; i++) // hier werden die Parameter zusammengebaut
        {
            values[i] = attribute[i].getDatentyp().getRandomValue();
            sb.append(values[i]);
            if (i<(attribute.length-1)) {
                sb.append(", ");
            }
        }
        
        sb.append(");\n");            
        
        for (int i=0; i<attribute.length; i++) //die Zeilen zur Uebrpruefung der Belegung
        {
            String expected = values[i];
            String actual = "";
            if (!klasse.privateAccess) actual = "testObject."+attribute[i].getBezeichner();
            else if (klasse.getters) actual = "testObject.get"+capitalise(attribute[i].getBezeichner()+"()");
            else {
                sb.append("    // ohne Getter und mit privaten Attributen Keine Moeglichkeit, den Konstruktor sinnvoll weiter zu testen\r\n");
                return sb.toString();
            }
            sb.append("    "+attribute[i].getDatentyp().generateAssertionCode(expected, actual));            
        }
        
        return sb.toString();
    }

    @Override
    String alsUML() {
        String result = name + "(";
        for (int i=0; i<attribute.length; i++) {
            result = result + attribute[i].getDatentyp().getName();
            if (i<(attribute.length-1)) {
                result = result + ", ";
            }
        }        
        result = result + ")";
        return result;
    }


}
    