package kiuziu.aufgabengeber.zwei;


public class IntGerade extends Methode
{

    public IntGerade()
    {
        super("istGerade", new DatentypBoolean(), new DatentypInt() );
        testname = "istGeradeInt";
    }

    @Override
	String beschreibungGeben() {
        String s = "Die Methode '"+this.alsUML()+"' nimmt ein int als Eingangsargument \r\n";
        s = s + "und gibt 'true' zurueck, wenn die Zahl gerade ist, sonst 'false'.";
        return s;
    }

    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<5; i++) {
            int value = Integer.parseInt(argumente[0].getRandomValue());
            String expected = "false";
            if (value%2==0) expected = "true"; 
            String actual = "testObject."+this.name+"("+value+")";
            sb.append("    "+returnTyp.generateAssertionCode(expected, actual));            
        }
        return sb.toString();
    }   

}
