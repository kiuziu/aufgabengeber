package kiuziu.aufgabengeber.zwei;


public class Setter extends Methode
{
    String attributName;

    public Setter(String attributName, Datentyp attributeType)
    {
        super("set"+attributName.substring(0,1).toUpperCase()+attributName.substring(1), new DatentypVoid(), attributeType );
        this.attributName = attributName;
    }

    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        String expected = "";
        String actual = "\"\"";
        if (!klasse.privateAccess) actual = "testObject."+attributName;
        else if (klasse.getters) actual = "testObject.get"+capitalise(attributName)+"()";

        for (int i=0; i<5; i++) {
            expected = argumente[0].getRandomValue();
            sb.append("    testObject."+this.name+"("+expected+");\r\n"); // call to setter method
            if (klasse.getters || !klasse.privateAccess) sb.append("    "+argumente[0].generateAssertionCode(expected, actual));            
        }
        return sb.toString();
    }   

}
