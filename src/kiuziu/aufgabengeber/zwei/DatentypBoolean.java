package kiuziu.aufgabengeber.zwei;

public class DatentypBoolean extends Datentyp {

	public DatentypBoolean() {
	}

	@Override
	String getName() {
		return "boolean";
	}

	@Override
	String getInitialValue() {
		return "false";
	}

	@Override
	String getRandomValue() {
		return Tools.randomOfAny("true", "false");
	}

}
