package kiuziu.aufgabengeber.zwei;


public class Getter extends Methode
{
    String attributName;

    public Getter(String attributName, Datentyp attributeType)
    {
        super("get"+attributName.substring(0,1).toUpperCase()+attributName.substring(1), attributeType);
        this.attributName = attributName;
    }
    
    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        String expected = "";
        String actual = "testObject."+this.name+"()";
        for (int i=0; i<5; i++) {
            expected = returnTyp.getRandomValue();
            if (!klasse.privateAccess) sb.append("    testObject."+attributName+" = "+expected+";\r\n"); // setting value           
            else if (klasse.setters) sb.append("    testObject.set"+capitalise(attributName)+"("+expected+");\r\n"); // setting value          
            
            if (klasse.setters || !klasse.privateAccess) sb.append("    "+returnTyp.generateAssertionCode(expected, actual));
            else sb.append("    "+returnTyp.getName()+" test"+i+" = "+actual+";\r\n"); 
       }
        return sb.toString();
    }   


}
