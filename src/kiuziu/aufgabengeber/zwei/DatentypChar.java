package kiuziu.aufgabengeber.zwei;


public class DatentypChar extends Datentyp
{

    public DatentypChar() {
    }

    @Override
	String getName() { return "char"; }

    @Override
	String getInitialValue() {
        return "'\u0000'";
    }

    @Override
	String getRandomValue() {
        return Tools.randomOfAny("'N'", "'S'", "'E'", "'W'", "'a'", "'b'", "'c'", "'d'", "'m'", "'w'", "'X'");
    }

}
