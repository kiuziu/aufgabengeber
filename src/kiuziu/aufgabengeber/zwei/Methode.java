package kiuziu.aufgabengeber.zwei;


public abstract class Methode //extends Methode
{
    String testname; // noetig bei ueberladenen Methoden, damit die Testmethode anders heisst
    String name; // bezeichner der Methode
    Datentyp returnTyp;
    Datentyp [] argumente;
    Klasse klasse;
    
    public Methode(String name) {
        this.name = name;
        this.testname = name;
        argumente = new Datentyp[0];
    }

    public Methode(String n, Datentyp r, Datentyp... a) {
        name = n;
        returnTyp = r;
        argumente = a;
        testname = n;
        if (a==null) argumente = new Datentyp[0];
    }
    
    void klasseSetzen(Klasse k) { klasse = k;}
    
    String alsUML() {                
        String result = name + "(";
        for (int i=0; i<argumente.length; i++) {
            result = result + argumente[i].getName();
            if (i<(argumente.length-1)) {
                result = result + ", ";
            }
        }        
        result = result + "): " + returnTyp.getName();
        return result;
    }
        
    String testCodeErzeugen() {
        StringBuilder sb = new StringBuilder();

        sb.append("  @Test\n");
        sb.append("  public void "+this.testname+"Test() {\r\n");
            
        sb.append(testzeilenErzeugen());
        
        sb.append("  }\r\n\r\n");
        return sb.toString();
    }   

    //helper, used by Getter and Setter when inferring methods
    String capitalise(String s) {
        return s.substring(0,1).toUpperCase()+s.substring(1);
    }

    String beschreibungGeben() { return ""; }

    abstract String testzeilenErzeugen();

}
