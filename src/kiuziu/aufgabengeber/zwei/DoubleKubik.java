package kiuziu.aufgabengeber.zwei;


public class DoubleKubik extends Methode
{

    public DoubleKubik()
    {
        super("hoch3", new DatentypDouble(), new DatentypDouble() );
        testname = "hoch3Double";
    }

    @Override
	String beschreibungGeben() {
        String s = "Die Methode '"+this.alsUML()+"' nimmt ein double als Eingangsargument \r\n";
        s = s + "und gibt den Eingangswert hoch 3 zurueck. (Das geht auch ohne Potenzrechnung.)";
        return s;
    }

    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<5; i++) {
            double value = Double.parseDouble(argumente[0].getRandomValue());
            String expected = ""+value*value*value;
            String actual = "testObject."+this.name+"("+value+")";
            sb.append("    "+returnTyp.generateAssertionCode(expected, actual));            
        }
        return sb.toString();
    }   

}
