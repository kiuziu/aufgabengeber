package kiuziu.aufgabengeber.zwei;


public class IntVerdoppeln extends Methode
{

    public IntVerdoppeln()
    {
        super("verdoppeln", new DatentypInt(), new DatentypInt() );
        testname = "verdoppelnInt";
    }

    @Override
	String beschreibungGeben() {
        String s = "Die Methode '"+this.alsUML()+"' nimmt ein int als Eingangsargument \r\n";
        s = s + "und gibt den doppelten Wert davon zurueck.";
        return s;
    }

    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<5; i++) {
            String value = argumente[0].getRandomValue();
            String expected = ""+Integer.parseInt(value)*2;
            String actual = "testObject."+this.name+"("+value+")";
            sb.append("    "+returnTyp.generateAssertionCode(expected, actual));            
        }
        return sb.toString();
    }   

}
