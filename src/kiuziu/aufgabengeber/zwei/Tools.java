package kiuziu.aufgabengeber.zwei;

public class Tools {
	
	private Tools() {}
	
	@SafeVarargs
	public static <T> T randomOfAny(T... x) {
		if (x == null || x.length == 0)
			throw new IllegalArgumentException("Illegal Argument!");
		return x[(int) (Math.random() * x.length)];
	}

}
