package kiuziu.aufgabengeber.zwei;

public class DatentypHaarfarbe extends DatentypString {
	
	@Override
	String getRandomValue() {
		return Tools.randomOfAny("\"blond\"", "\"schwarz\"", "\"rot\"", "\"weiss\"", "\"braun\"");
	}

}
