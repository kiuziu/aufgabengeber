package kiuziu.aufgabengeber.zwei;

public class Tester
{
    public Tester()
    {
        test();
    }
    
    void test() {        
        
               
        
        Klasse k = new Klasse("Mensch");
        Attribut att1 = new Attribut("haarfarbe", new DatentypHaarfarbe());
        Attribut att2 = new Attribut("plz", new DatentypPlz());
        Attribut att3 = new Attribut("groesse", new DatentypDouble());
        k.attributHinzufuegen(att1);
        k.attributHinzufuegen(att2);
        k.attributHinzufuegen(att3);
        k.attributHinzufuegen( new Attribut ("freund", new Klasse("Mensch")));
//         k.methodeHinzufuegen(new Setter("haarfarbe", new DatentypHaarfarbe()));
//         k.methodeHinzufuegen(new Getter("haarfarbe", new DatentypHaarfarbe()));
//         k.methodeHinzufuegen(new Setter("plz", new DatentypPLZ()));
//         k.methodeHinzufuegen(new Getter("plz", new DatentypPLZ()));

        boolean mitInitialisierung = true;
        k.ergaenzeKonstruktoren(mitInitialisierung);
//         k.methodeHinzufuegen( new Konstruktor(k.bezeichnerGeben())); // ohne Argumente
//         k.methodeHinzufuegen( new Konstruktor(k)); // alle Argumente
//         k.methodeHinzufuegen( new KonstruktorMitVorbelegung(k.bezeichnerGeben(), k.attributeGeben()));

//         k.ergaenzeGetterAusAttributen();
//         k.ergaenzeSetterAusAttributen();

        k.methodeHinzufuegen(new IntVerdoppeln());        
        k.methodeHinzufuegen(new IntBetrag());        
        k.methodeHinzufuegen(new DoubleKubik());        
        k.methodeHinzufuegen(new IntGerade());        
        k.methodeHinzufuegen(new IntArraySumme());        
        k.methodeHinzufuegen(new BegrenzterSetter("plz", 0, 5));
                
        boolean withComments = true;
        
        AufgabenErzeuger.erzeugeAufgabe(k, withComments);
        

    }


}
