package kiuziu.aufgabengeber.zwei;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 05.08.2017
 * @author 
 */

public class Fenster extends JFrame {

	private static final long serialVersionUID = 1L;

    private JLabel imageLabel = new JLabel();

    private JCheckBox withFullConstructor = new JCheckBox();
    private JCheckBox withAttributeInitialisation = new JCheckBox();
    private JCheckBox withGetterSetter = new JCheckBox();
    private JCheckBox withExtraMethods = new JCheckBox();
    private JCheckBox withPrivateAttributes = new JCheckBox();

    public Fenster() { 
        super();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        int frameWidth = 290+114+200; 
        int frameHeight = 404+20+30;
        setSize(frameWidth, frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width - getSize().width) / 2;
        int y = (d.height - getSize().height) / 2;
        setLocation(x, y);
        setTitle("Fenster");
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);

        imageLabel.setBounds(0, 0, 283+114+200, 369+20+30);
        cp.add(imageLabel);

        cp.add(addButton(16,32,185,49,"Ach was, 1 Attribut reicht",1,Color.BLACK, Color.WHITE));
        cp.add(addButton(32,88,209,57,"2 Attribute sollten es schon sein",2,Color.BLACK,Color.GREEN));
        cp.add(addButton(40,152,193,57,"Mutig: Ich wage mich an 3",3,Color.BLACK,Color.YELLOW));
        cp.add(addButton(24,216,193,57,"Der Wahnsinn! 4 Attribute!!!",4,Color.WHITE,Color.RED));

        withFullConstructor = addCheckBox(40, 288, "mit zwei Konstruktoren", false);
        cp.add(withFullConstructor);
        withAttributeInitialisation = addCheckBox(40, 312, "mit zu setzenden Startwerten", false);
        cp.add(withAttributeInitialisation);
        withGetterSetter = addCheckBox(40, 336, "Getter und Setter", true);
        cp.add(withGetterSetter);
        withExtraMethods = addCheckBox(40, 360, "Weitere Methoden", false);
        cp.add(withExtraMethods);
        withPrivateAttributes = addCheckBox(40+333, 336, "Attribute sind private", false);
        cp.add(withPrivateAttributes);

        setVisible(true);

        additionalSetup();
    }

    private JCheckBox addCheckBox(int x, int y, String text, boolean selected) {
    	JCheckBox box = new JCheckBox(text);
        box.setBounds(x, y, 300, 20);
        box.setOpaque(false);
        box.setSelected(selected);
		return box;
	}

	private Component addButton(int x, int y, int width, int height, 
    		String text, int attributeCount, Color foreground, Color background) {
    	JButton button = new JButton(text);
        button.setBounds(x,y,width,height);
        button.setMargin(new Insets(2, 2, 2, 2));
        button.addActionListener(e -> constructExercise(attributeCount));
        button.setBackground(background);
        button.setForeground(foreground);
        button.setFocusable(false);
		return button;
	}

	// Anfang Methoden

    public static void main(String[] args) {
        new Fenster();
    } // end of main

    private void additionalSetup() {
        drawImage("hintergrund.png", imageLabel);        
        getContentPane().remove(imageLabel);
        getContentPane().add(imageLabel);
        setTitle("Der Aufgabengeber");

        withPrivateAttributes.addActionListener(e -> {
        	if (withPrivateAttributes.isSelected()) {
                withGetterSetter.setSelected(true);                    
                withFullConstructor.setSelected(false);                    
                withAttributeInitialisation.setSelected(false);                    
            }
        });

        withAttributeInitialisation.addActionListener(e -> {
        	if (withAttributeInitialisation.isSelected()) withPrivateAttributes.setSelected(false); 
        });

        withGetterSetter.addActionListener(e -> {
        	if (!withAttributeInitialisation.isSelected()) withPrivateAttributes.setSelected(false);   
        });

        withFullConstructor.addActionListener(e -> {
        	if (withFullConstructor.isSelected()) withPrivateAttributes.setSelected(false); 
        });  
        
        repaint();
        validate();
    }

    public void constructExercise(int numberOfAttributes) {
        boolean fullConstructor = withFullConstructor.isSelected();
        boolean withInitialisation = withAttributeInitialisation.isSelected();
        boolean getter = withGetterSetter.isSelected();
        boolean setter = withGetterSetter.isSelected();
        boolean privateAccess = withPrivateAttributes.isSelected();
        boolean extraMethods = withExtraMethods.isSelected();

        boolean withComments = true;
        
        Controller c = new Controller();
        c.aufgabeErzeugen(numberOfAttributes, withInitialisation, fullConstructor, 
        		getter,  setter, extraMethods, withComments, privateAccess);
    }

    private void drawImage(String bildname, JLabel comp) {
        int breite = comp.getWidth();
        int hoehe = comp.getHeight();
        comp.setIcon(GraphicsHelper.resizeImageIcon(GraphicsHelper.createImageIcon(bildname),breite,hoehe));              
    }

}
