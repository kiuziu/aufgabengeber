package kiuziu.aufgabengeber.zwei;

public class IntArraySumme extends Methode
{

    public IntArraySumme()
    {
        super("summeVon"); //testname
    }
    
    @Override
	String beschreibungGeben() {
        String s = "Die Methode '"+this.alsUML()+"' nimmt ein int-Array als Eingangsargument \r\n";
        s = s + "und gibt die Summe aller Elemente des Arrays zurueck. \r\n";
        s = s + "(Wenn das Array die Laenge 0 hat: dann auch 0.)";
        return s;
    }
    
    @Override
    String alsUML() {
        return "summeVon(int []) : int";
    }

    @Override
	String testzeilenErzeugen() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<3; i++) {
            
            int length = (int) (Math.random()*8+1);
            if (i==0) length = 0;
            int [] test = new int[length];
            int summe = 0;
            
            sb.append("    int [] test"+i+" = new int["+length+"];\r\n");
            for (int j=0; j<length; j++) {
                test[j] = (int) (Math.random()*1000-500); 
                summe = summe + test[j];
                sb.append("    test"+i+"["+j+"] = "+test[j]+";\r\n");
            }
            
            String expected = ""+summe;
            String actual = "testObject.summeVon(test"+i+")";
            sb.append("    "+(new DatentypInt()).generateAssertionCode(expected, actual));            
            sb.append("\r\n");
        }
        return sb.toString();
    }   


}
