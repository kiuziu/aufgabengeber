package kiuziu.aufgabengeber.zwei;

public class KlassendiagrammErzeuger
{

    private static int getMaxLength(Klasse k) {
        int max = 0;
        if (k.bezeichnerGeben().length()>max) max = k.bezeichnerGeben().length();
        for (Attribut a : k.attributeGeben()) {
            int l = (a.getBezeichner()+": "+a.getDatentyp().getName()).length();
            if (l> max) max = l;
        }
        for (Methode m : k.methodenGeben()) {
            int l = m.alsUML().length();
            if (l>max) max = l;
        }
        return max;
    }

    private static String getBorderLine(int length) {
        StringBuilder result = new StringBuilder();
        for (int i=0; i<length; i++) result.append("-");
        return result.toString();        
    }

    private static String getBorder() {
        return getBorderLine(maxLength);
    }

    private static String fillLineUp(String s, int lineLength) {
        StringBuilder result = new StringBuilder();
        result.append(s);
        for (int i=0;i<lineLength-s.length();i++) {
            result.append(" ");
        }
        return result.toString();        
    }

    private static String createLine(String s, int length) {
        return "| "+fillLineUp(s, length)+" |\r\n";
    }

    private static StringBuilder result;
    private static int maxLength;

    private static void add(String s) {
        result.append(createLine(s, maxLength));
    }

    static String erzeugeDiagramm(Klasse k) {
        result = new StringBuilder();
        maxLength = getMaxLength(k);

        add(getBorder());
        add(k.bezeichnerGeben()); // Klassenname
        add(getBorder());
        for (Attribut a : k.attributeGeben()) {
            add(a.alsUML()); // Attribute
        }
        add(getBorder());
        for (Methode m : k.methodenGeben()) {
            add(m.alsUML()); //Methoden
        }
        add(getBorder());
        return result.toString();
    }
}
