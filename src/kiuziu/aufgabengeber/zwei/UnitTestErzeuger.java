package kiuziu.aufgabengeber.zwei;

public class UnitTestErzeuger
{
    static StringBuilder sb;
    
    static String erzeugeUnitTest(Klasse klasse) {
        sb = new StringBuilder();
        addIntro(klasse.bezeichnerGeben());
        for (Methode m : klasse.methodenGeben()) {
            sb.append(m.testCodeErzeugen());
        }
        addOutro();
        return sb.toString();        
    }

    private static void addIntro(String klassenname)
    {
        //imports
        addLine("import static org.junit.Assert.*;");
        addLine("import org.junit.After;");
        addLine("import org.junit.Before;");
        addLine("import org.junit.Test;");
        addLine("");
        //begin class frame
        addLine("public class "+klassenname+"Test {");
        addLine("");
        //attributes
        addLine("  "+klassenname+" testObject;");
        addLine("");        
        //constructor
        addLine("  public "+klassenname+"Test() {");
        addLine("  }");
        addLine("");
        //before
        addLine("  @Before");
        addLine("  public void setUp() {");

        // simple constructor must exist to create test object
        addLine("    testObject = new "+klassenname+"();");

        addLine("  }");
        addLine("");
        //after
        addLine("  @After");
        addLine("  public void tearDown() {");
        addLine("    testObject=null;");
        addLine("  }");
        addLine("");
    }

    private static void addOutro() {
        // end class frame
        addLine("}");
    }    

    private static void addLine(String s) {
        sb.append(s+"\r\n");
    }

}
