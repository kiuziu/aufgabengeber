package kiuziu.aufgabengeber;

public class ClassDiagramGenerator {
	static FileWriter writer = new FileWriter();

	static void saveDiagram(PseudoClass pseudoClass) {
		saveDiagram(pseudoClass, false);
	}

	static void saveDiagram(PseudoClass pseudoClass, boolean withInitialisation) {
		if (withInitialisation) {
			writer.createFile(pseudoClass.getName() + "Diagramm.txt", pseudoClass.asDiagramPlusInitialValues());
		} else {
			writer.createFile(pseudoClass.getName() + "Diagramm.txt", pseudoClass.asDiagram());
		}
	}

	static void saveDiagram(PseudoClass[] pseudoClasses) {
		saveDiagram(pseudoClasses, false);
	}

	static void saveDiagram(PseudoClass[] pseudoClasses, boolean withInitialisation) {
		for (PseudoClass p : pseudoClasses) {
			saveDiagram(p, withInitialisation);
		}
	}
}
