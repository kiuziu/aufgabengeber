package kiuziu.aufgabengeber;

public class Attribute {
	String name;
	String dataType;
	String value;

	public Attribute(String n, String d) {
		name = n;
		dataType = d;
		value = DataTypes.getSafeRandomExampleValue(dataType);
	}

	public Attribute(String n, String d, String v) {
		name = n;
		dataType = d;
		value = v;
	}

	public String getDataType() {
		return dataType;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return name + ": " + dataType;
	}

}
