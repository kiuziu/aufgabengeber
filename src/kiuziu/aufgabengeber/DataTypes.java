package kiuziu.aufgabengeber;

import static kiuziu.aufgabengeber.Tools.randomOfAny;

public class DataTypes {

	public DataTypes() {
	}

	static String getRandomExampleValue(String type) {
		String value = "null";
		if (type.equals("String")) {
			value = randomOfAny("\"Max\"", "\"Susi\"", "\"Shamira\"", "\"Anton\"", "\"Lydia\"", "\"London\"");
		} else if (type.equals("int")) {
			value = "" + (int) (Math.random() * 100 - 30);
		} else if (type.equals("boolean")) {
			value = randomOfAny("true", "false");
		} else if (type.equals("char")) {
			value = randomOfAny("'N'", "'S'", "'E'", "'W'", "'x'");
		} else if (type.equals("double")) {
			value = "" + (Math.random() * 10 - 3);
		}
		return value;
	}

	static String getInitialValue(String type) {
		String value = "null";
		if (type.equals("String"))
			value = "null";
		else if (type.equals("int"))
			value = "0";
		else if (type.equals("boolean"))
			value = "false";
		else if (type.equals("char"))
			value = "'\u0000'";
		else if (type.equals("double"))
			value = "0.0";
		return value;
	}

	static String getSafeRandomExampleValue(String type) {
		String result = getRandomExampleValue(type);
		while (result.equals(getInitialValue(type))) {
			result = getRandomExampleValue(type);
		}
		return result;
	}

	static boolean isLegalClassName(String name) {
		// to do: parse
		if(name != null) return true;
		return false;
	}

	static boolean isLegalAttribute(Attribute a) {
		// to do: parse
		if(a != null) return true;
		return false;
	}

}
