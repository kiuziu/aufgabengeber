package kiuziu.aufgabengeber;

import java.util.List;
import java.util.LinkedList;

public class PseudoClass {
	private String className;
	private List<Attribute> attributes = new LinkedList<>();
	private List<Constructor> constructors = new LinkedList<>();
	private List<Method> methods = new LinkedList<>();

	public PseudoClass() {
	}

	public void addConstructor(Constructor c) {
		constructors.add(c);
	}

	public void addAttribute(Attribute a) {
		attributes.add(a);
	}

	public void addMethod(Method m) {
		methods.add(m);
	}

	public void setName(String s) {
		className = s;
	}

	public String getName() {
		return className;
	}

	public List<Method> getMethods() {
		return methods;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public List<Constructor> getConstructors() {
		return constructors;
	}

	public String asDiagram(boolean initialValues) {
		StringBuilder result = new StringBuilder();

		result.append("|--------------------\r\n");

		result.append("| " + getName() + "\r\n");
		result.append("|--------------------\r\n");

		for (Attribute a : attributes) {
			result.append("| " + a.toString());
			result.append("\r\n");
		}
		result.append("|--------------------\r\n");

		for (Constructor c : constructors) {
			result.append("| " + c.toString());
			result.append("\r\n");
		}

		for (Method m : methods) {
			result.append("| " + m.toString());
			result.append("\r\n");
		}
		result.append("|--------------------\r\n");

		if (initialValues) {
			result.append("\r\n");
			result.append("\r\n");
			for (Attribute a : attributes) {
				result.append("Der Wert des Attributs '" + a.getName() + "' soll am Anfang sein: " + a.getValue());
				result.append("\r\n");
			}
		}
		return result.toString();
	}

	public String asDiagramPlusInitialValues() {
		return asDiagram(true);
	}

	public String asDiagram() {
		return asDiagram(false);
	}
}
