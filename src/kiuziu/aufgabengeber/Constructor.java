package kiuziu.aufgabengeber;

public class Constructor {
	String name;
	Attribute[] arguments;

	public Constructor(String n) {
		name = n;
		arguments = new Attribute[0];
	}

	public Constructor(String n, Attribute[] a) {
		name = n;
		arguments = a;
	}

	public Attribute[] getArguments() {
		return arguments;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		String result = name + "(";
		for (int i = 0; i < arguments.length; i++) {
			result += arguments[i].dataType;
			if (i < (arguments.length - 1)) {
				result += ", ";
			}
		}
		result += ")";
		return result;
	}

}
