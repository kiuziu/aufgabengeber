Aufgabe: Setze das folgende Klassendiagramm in eine Java-Klasse um.
Beachte dabei auch eventuelle zusaetzliche Angaben unter dem Diagramm.

| ---------------------------------- |
| Fahrzeug                           |
| ---------------------------------- |
| haarfarbe: String                  |
| haeufigkeit: double                |
| alter: int                         |
| postleitzahl: int                  |
| ---------------------------------- |
| Fahrzeug()                         |
| Fahrzeug(String, double, int, int) |
| getHaarfarbe(): String             |
| getHaeufigkeit(): double           |
| getAlter(): int                    |
| getPostleitzahl(): int             |
| setHaarfarbe(String): void         |
| setHaeufigkeit(double): void       |
| setAlter(int): void                |
| setPostleitzahl(int): void         |
| verdoppeln(int): int               |
| ---------------------------------- |


Der Wert des Attributs 'haarfarbe' soll nach Ausfuehrung 
des Konstruktors ohne Argumente sein: "rot".
Der Wert des Attributs 'haeufigkeit' soll nach Ausfuehrung 
des Konstruktors ohne Argumente sein: 8.952.
Der Wert des Attributs 'alter' soll nach Ausfuehrung 
des Konstruktors ohne Argumente sein: 44.
Der Wert des Attributs 'postleitzahl' soll nach Ausfuehrung 
des Konstruktors ohne Argumente sein: 31593.


Die Methode 'verdoppeln(int): int' nimmt ein int als Eingangsargument 
und gibt den doppelten Wert davon zurueck.

