package kiuziu.aufgabengeber.test;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FahrzeugTest {

  Fahrzeug testObject;

  public FahrzeugTest() {
  }

  @Before
  public void setUp() {
    testObject = new Fahrzeug();
  }

  @After
  public void tearDown() {
    testObject=null;
  }

  @Test
  public void FahrzeugKonstruktorMitInitialisierungTest() {
    testObject = new Fahrzeug();
    assertEquals("rot", testObject.haarfarbe);
    assertEquals(8.952, testObject.haeufigkeit, 0.0005);
    assertEquals(44, testObject.alter);
    assertEquals(31593, testObject.postleitzahl);
  }

  @Test
  public void FahrzeugKonstruktorMitAllenArgumentenTest() {
    testObject = new Fahrzeug("blond", 24.88, 42, 41986);
    assertEquals("blond", testObject.haarfarbe);
    assertEquals(24.88, testObject.haeufigkeit, 0.0005);
    assertEquals(42, testObject.alter);
    assertEquals(41986, testObject.postleitzahl);
  }

  @Test
  public void getHaarfarbeTest() {
    testObject.haarfarbe = "blond";
    assertEquals("blond", testObject.getHaarfarbe());
    testObject.haarfarbe = "braun";
    assertEquals("braun", testObject.getHaarfarbe());
    testObject.haarfarbe = "braun";
    assertEquals("braun", testObject.getHaarfarbe());
    testObject.haarfarbe = "rot";
    assertEquals("rot", testObject.getHaarfarbe());
    testObject.haarfarbe = "braun";
    assertEquals("braun", testObject.getHaarfarbe());
  }

  @Test
  public void getHaeufigkeitTest() {
    testObject.haeufigkeit = 54.96;
    assertEquals(54.96, testObject.getHaeufigkeit(), 0.0005);
    testObject.haeufigkeit = 20.30;
    assertEquals(20.30, testObject.getHaeufigkeit(), 0.0005);
    testObject.haeufigkeit = -26.4;
    assertEquals(-26.4, testObject.getHaeufigkeit(), 0.0005);
    testObject.haeufigkeit = 60.66;
    assertEquals(60.66, testObject.getHaeufigkeit(), 0.0005);
    testObject.haeufigkeit = 33.38;
    assertEquals(33.38, testObject.getHaeufigkeit(), 0.0005);
  }

  @Test
  public void getAlterTest() {
    testObject.alter = 59;
    assertEquals(59, testObject.getAlter());
    testObject.alter = 43;
    assertEquals(43, testObject.getAlter());
    testObject.alter = 40;
    assertEquals(40, testObject.getAlter());
    testObject.alter = 1;
    assertEquals(1, testObject.getAlter());
    testObject.alter = -24;
    assertEquals(-24, testObject.getAlter());
  }

  @Test
  public void getPostleitzahlTest() {
    testObject.postleitzahl = 55574;
    assertEquals(55574, testObject.getPostleitzahl());
    testObject.postleitzahl = 21479;
    assertEquals(21479, testObject.getPostleitzahl());
    testObject.postleitzahl = 65970;
    assertEquals(65970, testObject.getPostleitzahl());
    testObject.postleitzahl = 73727;
    assertEquals(73727, testObject.getPostleitzahl());
    testObject.postleitzahl = 38619;
    assertEquals(38619, testObject.getPostleitzahl());
  }

  @Test
  public void setHaarfarbeTest() {
    testObject.setHaarfarbe("schwarz");
    assertEquals("schwarz", testObject.haarfarbe);
    testObject.setHaarfarbe("schwarz");
    assertEquals("schwarz", testObject.haarfarbe);
    testObject.setHaarfarbe("blond");
    assertEquals("blond", testObject.haarfarbe);
    testObject.setHaarfarbe("rot");
    assertEquals("rot", testObject.haarfarbe);
    testObject.setHaarfarbe("schwarz");
    assertEquals("schwarz", testObject.haarfarbe);
  }

  @Test
  public void setHaeufigkeitTest() {
    testObject.setHaeufigkeit(56.84);
    assertEquals(56.84, testObject.haeufigkeit, 0.0005);
    testObject.setHaeufigkeit(68.16);
    assertEquals(68.16, testObject.haeufigkeit, 0.0005);
    testObject.setHaeufigkeit(1.064);
    assertEquals(1.064, testObject.haeufigkeit, 0.0005);
    testObject.setHaeufigkeit(53.76);
    assertEquals(53.76, testObject.haeufigkeit, 0.0005);
    testObject.setHaeufigkeit(23.27);
    assertEquals(23.27, testObject.haeufigkeit, 0.0005);
  }

  @Test
  public void setAlterTest() {
    testObject.setAlter(-24);
    assertEquals(-24, testObject.alter);
    testObject.setAlter(1);
    assertEquals(1, testObject.alter);
    testObject.setAlter(44);
    assertEquals(44, testObject.alter);
    testObject.setAlter(-16);
    assertEquals(-16, testObject.alter);
    testObject.setAlter(16);
    assertEquals(16, testObject.alter);
  }

  @Test
  public void setPostleitzahlTest() {
    testObject.setPostleitzahl(83457);
    assertEquals(83457, testObject.postleitzahl);
    testObject.setPostleitzahl(48609);
    assertEquals(48609, testObject.postleitzahl);
    testObject.setPostleitzahl(90964);
    assertEquals(90964, testObject.postleitzahl);
    testObject.setPostleitzahl(81903);
    assertEquals(81903, testObject.postleitzahl);
    testObject.setPostleitzahl(47754);
    assertEquals(47754, testObject.postleitzahl);
  }

  @Test
  public void verdoppelnIntTest() {
    assertEquals(-26, testObject.verdoppeln(-13));
    assertEquals(136, testObject.verdoppeln(68));
    assertEquals(18, testObject.verdoppeln(9));
    assertEquals(-54, testObject.verdoppeln(-27));
    assertEquals(74, testObject.verdoppeln(37));
  }

}
