package kiuziu.aufgabengeber.test;

public class Fahrzeug {
	String haarfarbe;
	double haeufigkeit;
	int alter;
	int postleitzahl;
	
	public Fahrzeug() {
		haarfarbe = "rot";
		haeufigkeit = 8.952;
		alter = 44;
		postleitzahl = 31593;
	}
	
	public Fahrzeug(String haarfarbe, double haeufigkeit, int alter, int postleitzahl) {
		this.haarfarbe = haarfarbe;
		this.haeufigkeit = haeufigkeit;
		this.alter = alter;
		this.postleitzahl = postleitzahl;
	}

	public String getHaarfarbe() {
		return haarfarbe;
	}

	public void setHaarfarbe(String haarfarbe) {
		this.haarfarbe = haarfarbe;
	}

	public double getHaeufigkeit() {
		return haeufigkeit;
	}

	public void setHaeufigkeit(double haeufigkeit) {
		this.haeufigkeit = haeufigkeit;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}

	public int getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(int postleitzahl) {
		this.postleitzahl = postleitzahl;
	}
	
	public int verdoppeln(int wert) {
		return wert * 2;
	}
}
