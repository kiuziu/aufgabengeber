package kiuziu.aufgabengeber;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 05.08.2017
 * @author Herr Rau@Lehrerzimmer
 * @version 1.1 vom 21.08.2017 Version 1.0 ist modifiziert durch
 * @author Manuel Prochnow
 */

public class Fenster extends JFrame {

	private static final long serialVersionUID = 1L;

    private JLabel mainProgramWindow = new JLabel();
    private JCheckBox checkBoxTwoConstructors;
    private JCheckBox checkBoxInitialValue;
    private JCheckBox checkBoxWithMethods;
    private JCheckBox checkBoxPrivateAttributs;
    
    private int width = 610;
    private int height = 450;

    public Fenster() { 
        super();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setFrameSize(width, height);
        posFrameInTheMiddle();
        setTitle("Der Klassendiagramm-Implementierungs-Aufgabengeber");
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);

        mainProgramWindow.setBounds(0, 0, width, height);
        cp.add(mainProgramWindow);
        
        addButton(cp, "Ach was, 1 Attribut reicht", new Rectangle(16, 32, 185, 49), 1, Color.BLACK, Color.WHITE);
        addButton(cp, "2 Attribute sollten es schon sein", new Rectangle(32, 88, 209, 57), 2, Color.BLACK, Color.GREEN);
        addButton(cp, "Mutig: Ich wage mich an 3", new Rectangle(40, 152, 193, 57), 3, Color.BLACK, Color.YELLOW);
        addButton(cp, "Der Wahnsinn! 4 Attribute!!!", new Rectangle(24, 216, 193, 57), 4, Color.WHITE, Color.RED);

        checkBoxTwoConstructors = addCheckBox(cp, "mit zwei Konstruktoren", new Rectangle(40, 288, 152, 20), false);
        cp.add(checkBoxTwoConstructors);
        
        checkBoxInitialValue = addCheckBox(cp, "mit zu setzenden Startwerten", new Rectangle(40, 312, 179, 20), false);
        cp.add(checkBoxInitialValue);
        
        checkBoxWithMethods = addCheckBox(cp, "Mit Methoden", new Rectangle(40, 336, 206, 20), true);
        cp.add(checkBoxWithMethods);
        
        checkBoxPrivateAttributs = addCheckBox(cp, "Attribute sind private", new Rectangle(373, 336, 206, 20), false);
        cp.add(checkBoxPrivateAttributs);
     
        drawImage("hintergrund.png", mainProgramWindow);   
        
        cp.remove(mainProgramWindow);
        cp.add(mainProgramWindow);
        
        repaint();
        validate();
        
        setVisible(true);
    }

	private void posFrameInTheMiddle() {
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width - getSize().width) / 2;
        int y = (d.height - getSize().height) / 2;
        setLocation(x, y);
	}

	private void setFrameSize(int x, int y) {
		int frameWidth = x; 
        int frameHeight = y;
        setSize(frameWidth, frameHeight);
	}

    @SuppressWarnings("unused")
	public static void main(String[] args) {
        new Fenster();
    }
    
    private JCheckBox addCheckBox(Container c, String text, Rectangle r, boolean selected){
    	JLabel boxText = new JLabel(text);
    	boxText.setBounds(r.x + 24, r.y, r.width, r.height);
    	c.add(boxText);
    	JCheckBox result = new JCheckBox();
    	result.setBounds(r);
    	result.setOpaque(false);
    	result.setSelected(selected);
		return result;
    }

    private void addButton(Container c, String text, Rectangle r, int numberOfAttributes, Color fg, Color bg) {
    	JButton result = new JButton(text);
        result.setBounds(r);
        result.setMargin(new Insets(2, 2, 2, 2));
        result.addActionListener(e -> constructExercise(numberOfAttributes));
        result.setForeground(fg);
        result.setBackground(bg);
        result.setFocusable(false);
        c.add(result);
    }

    ClassGenerator cg = new ClassGenerator();
    PseudoClass pc = null;

    public void constructExercise(int numberOfAttributes) {
        boolean fullConstructor = checkBoxTwoConstructors.isSelected();
        boolean checkInitialisation = checkBoxInitialValue.isSelected();
        boolean getter = checkBoxWithMethods.isSelected();
        boolean setter = checkBoxWithMethods.isSelected();
        boolean attributeAccess = !checkBoxPrivateAttributs.isSelected();
        PseudoClass pc = ClassGenerator.generateRandomClass(numberOfAttributes, true, fullConstructor, getter, setter);
        Parameter p = new Parameter(pc, attributeAccess, fullConstructor, getter, setter, checkInitialisation);
        UnitTestGenerator.saveUnitTest(p);
        ClassDiagramGenerator.saveDiagram(pc, checkInitialisation);
    }

    private void drawImage(String bildname, JLabel comp) {
        int breite = comp.getWidth();
        int hoehe = comp.getHeight();
        comp.setIcon(GraphicsHelper.resizeImageIcon(GraphicsHelper.createImageIcon(bildname),breite,hoehe));              
    }

}
