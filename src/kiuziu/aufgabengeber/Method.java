package kiuziu.aufgabengeber;

public class Method
{
    String name;
    String returnType;
    Attribute [] arguments;    
    
    public Method(String n, String r, Attribute [] a) {
        name = n;
        returnType = r;
        arguments = a;
        if (a==null) arguments = new Attribute[0];    
    }
    
    public String getName() { return name; }

    public String getReturnType() { return returnType; }

    public Attribute[] getArguments() { return arguments; }
    
    @Override
    public String toString() {
        String result = name + "(";
        for (int i=0; i<arguments.length; i++) {
            result = result + arguments[i].dataType;
            if (i<(arguments.length-1)) {
                result = result + ", ";
            }
        }        
        result = result + "): " + returnType;
        return result;
    }

}
