import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MenschTest {

  Mensch testObject;

  public MenschTest() {
  }

  @Before
  public void setUp() {
    testObject = new Mensch();
  }

  @After
  public void tearDown() {
    testObject=null;
  }

  @Test
  public void MenschKonstruktorMitInitialisierungTest() {
    testObject = new Mensch();
    assertEquals("rot", testObject.haarfarbe);
    assertEquals(59921, testObject.plz);
    assertEquals(29.11, testObject.groesse, 0.0005);
    assertNotNull(testObject.freund);
  }

  @Test
  public void MenschKonstruktorMitAllenArgumentenTest() {
    testObject = new Mensch("schwarz", 20554, 56.06, new Mensch());
    assertEquals("schwarz", testObject.haarfarbe);
    assertEquals(20554, testObject.plz);
    assertEquals(56.06, testObject.groesse, 0.0005);
    assertNotNull(testObject.freund);
  }

  @Test
  public void verdoppelnIntTest() {
    assertEquals(-14, testObject.verdoppeln(-7));
    assertEquals(10, testObject.verdoppeln(5));
    assertEquals(50, testObject.verdoppeln(25));
    assertEquals(2, testObject.verdoppeln(1));
    assertEquals(-30, testObject.verdoppeln(-15));
  }

  @Test
  public void betragIntTest() {
    assertEquals(14, testObject.betrag(14));
    assertEquals(19, testObject.betrag(19));
    assertEquals(27, testObject.betrag(27));
    assertEquals(28, testObject.betrag(28));
    assertEquals(3, testObject.betrag(-3));
  }

  @Test
  public void hoch3DoubleTest() {
    assertEquals(211708.73600000003, testObject.hoch3(59.6), 0.0005);
    assertEquals(235227.89971699996, testObject.hoch3(61.73), 0.0005);
    assertEquals(-177.50432800000002, testObject.hoch3(-5.62), 0.0005);
    assertEquals(42362.55525599999, testObject.hoch3(34.86), 0.0005);
    assertEquals(44136.67730400001, testObject.hoch3(35.34), 0.0005);
  }

  @Test
  public void istGeradeIntTest() {
    assertEquals(false, testObject.istGerade(55));
    assertEquals(true, testObject.istGerade(-18));
    assertEquals(true, testObject.istGerade(22));
    assertEquals(true, testObject.istGerade(54));
    assertEquals(true, testObject.istGerade(-20));
  }

  @Test
  public void summeVonTest() {
    int [] test0 = new int[0];
    assertEquals(0, testObject.summeVon(test0));

    int [] test1 = new int[2];
    test1[0] = 248;
    test1[1] = 86;
    assertEquals(334, testObject.summeVon(test1));

    int [] test2 = new int[2];
    test2[0] = -381;
    test2[1] = 46;
    assertEquals(-335, testObject.summeVon(test2));

  }

  @Test
  public void setPlzTest() {
    testObject.plz = 0;
    testObject.setPlz(-2147483648);
    assertEquals(0, testObject.plz);

    testObject.plz = 0;
    testObject.setPlz(-1);
    assertEquals(0, testObject.plz);

    testObject.plz = 0;
    testObject.setPlz(3);
    assertEquals(3, testObject.plz);

    testObject.plz = 1;
    testObject.setPlz(1);
    assertEquals(1, testObject.plz);

    testObject.plz = 3;
    testObject.setPlz(5);
    assertEquals(3, testObject.plz);

    testObject.plz = 0;
    testObject.setPlz(2046657121);
    assertEquals(0, testObject.plz);

  }

}
