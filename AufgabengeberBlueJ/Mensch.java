
public class Mensch
{
    String haarfarbe;
    int plz;
    double groesse;
    Mensch freund;
    
    public Mensch()
    {
        haarfarbe = "blond";
        plz = 23691;
    }
    
    public Mensch(String s, int i, double d)
    {
        haarfarbe = s;
        plz = i;
        groesse = d;
    }
    
    public Mensch(String s, int i, double d, Mensch m)
    {
        this(s,i,d);
        freund = m;
    }
    
    void setHaarfarbe(String s) {
        haarfarbe = s;
    }

    String getHaarfarbe() {
        return haarfarbe;
    }

    void setFreund(Mensch m) {
        freund = m;
    }

    Mensch getFreund() {
        return freund;
    }

    void setGroesse(double d) {
        groesse = d;
    }

    double getGroesse() {
        return groesse;
    }

    void setPlz(int i) {
        plz = i;
    }

    int getPlz() {
        return plz;
    }
    
    int verdoppeln(int i) {
        return i+i;
    }
    boolean istGerade(int i) {
        return i%2 == 0;
    }
    int betrag(int i) {
        return i;
    }
    double hoch3(double i) {
        return i*i*i;
    }
    
    int summeVon(int [] zahlen) {
        int erg = 0;
        for (int i=0; i<zahlen.length; i++) erg = erg+zahlen[i];
        return erg;
    }


}
