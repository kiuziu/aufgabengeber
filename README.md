# README #

Das Projekt "Der Java-Klassendiagramm-Implementierungs-Aufgaben-Generator" ist unter

https://www.herr-rau.de/wordpress/2017/08/der-java-klassendiagramm-implementierungs-aufgaben-generator.htm

zufinden.

Urheber und Author des Projekts ist Thomas Rau. Er unterrichte Deutsch, Informatik und Englisch an einem bayerischen Gymnasium. 
Außerdem arbeitet er als Fachdidaktiker für Informatik an der Ludwig-Maximilians-Universität München. 
Er ist über seine E-Mail lehrerzimmer@herr-rau.de zu erreichen.

Nun zu mir:
mein Name ist Manuel Prochnow. Ich wohne und arbeite in Berlin. 
Meine Hobbies sind programmieren in java, keine Projekt mit dem Arduino und dem Raspberry Pi. Der Arduino versteht C++ und Raspberry Pi Python.

Zu diesem Projekt bin ich beim Stöbern im Netz gekommen. 
Es war für die Lern-IDE Blue-J angelegt, da ich aber lieber mit Eclipse arbeite habe ich es kurzer Hand portiert.
Bei der Gelegenheit hatte ich einiges an refactoring-Potentential entdeckt. Bei der Analyse des Codes konnte ich noch ein paar Fehler beheben.

Ich bedanke mich vielmals für diese tolle Anregung beim Author. 
Noch ein Hinweis in eigener Sache:

Ich habe keinerlei finanzielle Ambitionen und ich will mich auf keinen Fall mit fremden Federn schmücken.
Die Idee stammt ausdrücklich nicht von mir, sondern vom o. g. Author.

Mich erreicht Ihr unter kiuziu@gmail.com